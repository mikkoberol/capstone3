const productData = [
    {
        id: "wdc001",
        name: "Infinix Hot 20s",
        description: "More Than a Phone, It's Your Ultimate Companion - Unleash Possibilities with Infinix Hot 20s.",
        price: 8499,
        onOffer: true
    },
    {
        id: "wdc002",
        name: "Redmi Note 11",
        description: "Crafted with Precision, Designed for You - Redmi Note 11: Where Style Meets Technology.",
        price: 9999,
        onOffer: true
    },
    {
        id: "wdc003",
        name: "Honor X7a",
        description: "Uncompromised Quality, Unmatched Experience - [Honor X7a]: Redefining Excellence.",
        price: 7990,
        onOffer: true
    },
    {
        id: "wdc004",
        name: "Tecno Spark 10 Pro",
        description: "Power, Performance, and Perfection - Choose [Tecno Spark 10 Pro] for Your Digital Journey.",
        price: 7499,
        onOffer: true
    }
]

export default productData;
