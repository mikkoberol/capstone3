import React, { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';
import UserContext from '../UserContext';

export default function Register() {
  const { user } = useContext(UserContext);
  const navigate = useNavigate();

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [isActive, setIsActive] = useState(false);

  useEffect(() => {
    setIsActive(email !== '' && password !== '' && confirmPassword !== '' && password === confirmPassword);
  }, [email, password, confirmPassword]);

  const registerUser = (e) => {
    e.preventDefault();

    fetch(`https://cpstn2-ecommerceapi-berol.onrender.com/users/register`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        email: email,
        password: password,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data) {
          setEmail('');
          setPassword('');
          setConfirmPassword('');

          alert('Thank you for registering!');

          navigate("/login");

        } else {

          alert('Please try again');
        }
      })
      .catch((error) => {

        console.error('Error registering user:', error);

        alert('Please try again!');
      });
  };

  return (
    <div className="container mt-5 text-center">
      <div className="row justify-content-center align-items-center">
        <div className="col-md-6 col-lg-4">
          <Form onSubmit={(e) => registerUser(e)}>
            <h1 className="my-5 text-center">Register</h1>

            <Form.Group>
              <Form.Label>Email:</Form.Label>
              <Form.Control
                className="m-1 text-center"
                type="email"
                placeholder="Enter Email"
                required
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
            </Form.Group>

            <Form.Group>
              <Form.Label>Password: </Form.Label>
              <Form.Control
                className="m-1 text-center"
                type="password"
                placeholder="Enter Password"
                required
                value={password}
                onChange={(e) => setPassword(e.target.value)}
              />
            </Form.Group>

            <Form.Group>
              <Form.Label>Confirm Password: </Form.Label>
              <Form.Control
                className="m-1 text-center"
                type="password"
                placeholder="Confirm Password"
                required
                value={confirmPassword}
                onChange={(e) => setConfirmPassword(e.target.value)}
              />
            </Form.Group>

            {isActive ? (
              <Button variant="primary" type="submit" id="submitBtn">
                Submit
              </Button>
            ) : (
              <Button variant="danger" type="submit" id="submitBtn" disabled>
                Please enter your registration details
              </Button>
            )}
          </Form>
        </div>
      </div>
    </div>
  );
}
