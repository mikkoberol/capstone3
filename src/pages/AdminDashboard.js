// import React, { useState, useEffect } from 'react';
// import { Table, Form, Button } from 'react-bootstrap';
// import Swal from 'sweetalert2';

//   const AdminDashboard = () => {

//     const [product, setProduct] = useState([]);
//     const [newProduct, setNewProduct] = useState({ name: '', description: '', price: 0, isActive: true });
//     const [editProduct, setEditProduct] = useState(null);

//     const fetchProduct = () => {
//       const token = localStorage.getItem('token');
      
//       if (token) {
//         fetch(`https://cpstn2-ecommerceapi-berol.onrender.com/product/all`, {
//           headers: {
//             Authorization: `Bearer ${token}`,
//           },
//         })
//           .then(response => response.json())
//           .then(data => {
//             console.log(data);
//             setProduct(data);
//           })
//           .catch(error => console.error('Error fetching products:', error));
//       } else {
//         console.error('User is not authenticated. No token found in localStorage.');
//       }
//     };

//     const handleSubmit = (e) => {
//     e.preventDefault();

//     const token = localStorage.getItem('token');

//     fetch(`https://cpstn2-ecommerceapi-berol.onrender.com/product/create`, {
//       method: 'POST',
//       headers: {
//         'Content-Type': 'application/json',
//         Authorization: `Bearer ${token}`,
//       },
//       body: JSON.stringify(newProduct),
//     })
//       .then(response => response.json())
//       .then(data => {
//         setNewProduct({ name: '', description: '', price: 0, isActive: true });
//         fetchProduct();

//         Swal.fire({
//           icon: 'success',
//           title: 'Product Added!',
//           text: 'The product has been successfully added.',
//         });
//       })
//       .catch(error => console.error('Error creating product:', error));
//   };

//   const handleToggleProduct = (productId, isActive) => {
//     const token = localStorage.getItem('token');

//     fetch(`https://cpstn2-ecommerceapi-berol.onrender.com/product/${productId}/${isActive ? 'archive' : 'activate'}`, {
//       method: 'PATCH',
//       headers: {
//         Authorization: `Bearer ${token}`,
//       },
//     })
//       .then(response => {
//         fetchProduct();
//       })
//       .catch(error => console.error('Error toggling product status:', error));
//   };


//   // const handleEditProduct = (product) => {
//   //   console.log('Editing Product:', product);

//   //   setEditProduct(product);

//   //   setNewProduct({
//   //     name: product.name,
//   //     description: product.description,
//   //     price: product.price,
//   //     //isActive: product.isActive,
//   //   });
//   // console.log('editProduct:', editProduct); // Check if editProduct is updated
//   // console.log('newProduct:', newProduct); // Check if newProduct is updated
//   // };

//   //TEST UPDATE PRODUCT

//   const handleEditProduct = (productId) => {
//   const token = localStorage.getItem('token');

//   fetch(`https://cpstn2-ecommerceapi-berol.onrender.com/product/${productId}`, {
//     method: 'PATCH',
//     headers: {
//       'Content-Type': 'application/json',  // Set content type to JSON
//       Authorization: `Bearer ${token}`,
//     },
//     body: JSON.stringify(newProduct),  // Send updated product data in the request body
//   })
//     .then(response => {
//       // Handle the response accordingly
//       if (response.ok) {
//         // Product updated successfully
//         fetchProduct();  // Fetch updated product list
//       } else {
//         // Handle errors
//         console.error('Error updating product:', response.statusText);
//       }
//     })
//     .catch(error => console.error('Error updating product:', error));
// };

// // // Call handleUpdateProduct with the product ID when you want to update a product
// // handleProduct(product._id);



//   const handleDeleteProduct = (productId) => {
//     const token = localStorage.getItem('token');

//     fetch(`https://cpstn2-ecommerceapi-berol.onrender.com/product/${productId}`, {
//       method: 'DELETE',
//       headers: {
//         Authorization: `Bearer ${token}`,
//       },
//     })
//       .then(response => {
//         fetchProduct();
//       })
//       .catch(error => console.error('Error deleting product:', error));
//   };

//   const handleClearForm = () => {
//     setEditProduct(null);
//     setNewProduct({ name: '', description: '', price: 0, isActive: true });
//   };

//   useEffect(() => {
//     fetchProduct();
//   }, []);

// // useEffect for editing product

//   useEffect(() => {
//     if (editProduct) {
//       setNewProduct({
//         name: editProduct.name,
//         description: editProduct.description,
//         price: editProduct.price,
//         isActive: editProduct.isActive,
//       });
//     } else {
//       handleClearForm();
//     }
//   }, [editProduct]);


//   return (
//     <div>
//       <h2>Admin Dashboard</h2>

//       <Button variant="primary" onClick={handleClearForm}>Add New Product</Button>
//       <br/>

//       <Table striped bordered hover>
//         <thead>
//           <tr>
//             <th>Name</th>
//             <th>Description</th>
//             <th>Price</th>
//             <th>Status</th>
//             <th>Actions</th>
//           </tr>
//         </thead>
//         <tbody>
//           {Array.isArray(product) && product.map((product) => (
//             <tr key={product._id}>
//               <td>{product.name}</td>
//               <td>{product.description}</td>
//               <td>${product.price}</td>
//               <td>{product.isActive ? 'Active' : 'Inactive'}</td>
//               <td>
//                 <Button variant="primary" onClick={() => handleEditProduct(product)}>
//                   Edit
//                 </Button>{' '}
//                 <Button
//                   variant={product.isActive ? 'danger' : 'success'}
//                   onClick={() => handleToggleProduct(product._id, product.isActive)}
//                 >
//                   {product.isActive ? 'Deactivate' : 'Reactivate'}
//                 </Button>{' '}
//                 <Button variant="danger" onClick={() => handleDeleteProduct(product._id)}>
//                   Delete
//                 </Button>
//               </td>
//             </tr>
//           ))}
//         </tbody>
//       </Table>

//       <Form onSubmit={handleSubmit}>
//         <Form.Group controlId="formName">
//           <Form.Label>Product Name</Form.Label>
//           <Form.Control
//             type="text"
//             placeholder="Enter product name"
//             value={newProduct.name}
//             onChange={(e) => setNewProduct({ ...newProduct, name: e.target.value })}
//             required
//           />
//         </Form.Group>
//         <Form.Group controlId="formDescription">
//           <Form.Label>Description</Form.Label>
//           <Form.Control
//             type="text"
//             placeholder="Enter product description"
//             value={newProduct.description}
//             onChange={(e) => setNewProduct({ ...newProduct, description: e.target.value })}
//             required
//           />
//         </Form.Group>
//         <Form.Group controlId="formPrice">
//           <Form.Label>Price</Form.Label>
//           <Form.Control
//             type="number"
//             placeholder="Enter product price"
//             value={newProduct.price}
//             onChange={(e) => setNewProduct({ ...newProduct, price: e.target.value })}
//             required
//           />
//         </Form.Group>
//         <Form.Group controlId="formStatus">
//           <Form.Check
//             type="checkbox"
//             label="Active"
//             checked={newProduct.isActive}
//             onChange={(e) => setNewProduct({ ...newProduct, isActive: e.target.checked })}
//           />
//         </Form.Group>
//         <Button variant="primary" type="submit">
//           {editProduct ? 'Update Product' : 'Add Product'}
//         </Button>
//       </Form>
//     </div>
//   );
// };

// export default AdminDashboard;


//TEST ADMIN DB Code

import React, { useState, useEffect } from 'react';
import { Table, Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

const AdminDashboard = () => {
  // States for managing products
  const [products, setProducts] = useState([]);
  const [newProduct, setNewProduct] = useState({ name: '', description: '', price: 0, isActive: true });
  const [editProduct, setEditProduct] = useState(null);

  // Function to fetch the list of products
  const fetchProducts = () => {
    const token = localStorage.getItem('token');
    
    if (token) {
      fetch(`https://cpstn2-ecommerceapi-berol.onrender.com/product/all`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
        .then(response => response.json())
        .then(data => {
          console.log(data); // Check if able to pull up necessary data
          setProducts(data);
        })
        .catch(error => console.error('Error fetching products:', error));
    } else {
      console.error('User is not authenticated. No token found in localStorage.');
    }
  };

  // Function to handle form submission for adding or updating a product
  const handleSubmit = (e) => {
    e.preventDefault();

    const token = localStorage.getItem('token');

    if (editProduct) {
      // Update existing product
      fetch(`https://cpstn2-ecommerceapi-berol.onrender.com/product/${editProduct._id}`, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`, // Include the token in the request headers
        },
        body: JSON.stringify(newProduct),
      })
        .then(response => {
          // Clear the form and reload the list of products
          setNewProduct({ name: '', description: '', price: 0, isActive: true });
          setEditProduct(null);
          fetchProducts(); // Fetch the updated list of products

          // Show sweet alert for successful update
          Swal.fire({
            icon: 'success',
            title: 'Product Updated!',
            text: 'The product has been successfully updated.',
          });
        })
        .catch(error => console.error('Error updating product:', error));
    } else {
      // Add new product
      fetch(`https://cpstn2-ecommerceapi-berol.onrender.com/product/create`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`, // Include the token in the request headers
        },
        body: JSON.stringify(newProduct),
      })
        .then(response => {
          // Clear the form and reload the list of products
          setNewProduct({ name: '', description: '', price: 0, isActive: true });
          fetchProducts(); // Fetch the updated list of products

          // Show sweet alert for successful addition
          Swal.fire({
            icon: 'success',
            title: 'Product Added!',
            text: 'The product has been successfully added.',
          });
        })
        .catch(error => console.error('Error creating product:', error));
    }
  };

  // Function to handle product deactivation/reactivation
  const handleToggleProduct = (productId, isActive) => {
    const token = localStorage.getItem('token');

    fetch(`https://cpstn2-ecommerceapi-berol.onrender.com/product/${productId}/${isActive ? 'archive' : 'activate'}`, {
      method: 'PATCH',
      headers: {
        Authorization: `Bearer ${token}`, // Include the token in the request headers
      },
    })
      .then(response => {
        fetchProducts(); // Fetch the updated list of products
      })
      .catch(error => console.error('Error toggling product status:', error));
  };

  // Function to handle product edit
  const handleEditProduct = (product) => {
    // Set the editProduct state to the selected product
    setEditProduct(product);

    // Set the newProduct state with the selected product's details
    setNewProduct({
      name: product.name,
      description: product.description,
      price: product.price,
      isActive: product.isActive,
    });
  };

  // Function to handle product delete
  const handleDeleteProduct = (productId) => {
    const token = localStorage.getItem('token');

    fetch(`https://cpstn2-ecommerceapi-berol.onrender.com/product/${productId}`, {
      method: 'DELETE',
      headers: {
        Authorization: `Bearer ${token}`, // Include the token in the request headers
      },
    })
      .then(response => {
        fetchProducts(); // Fetch the updated list of products
      })
      .catch(error => console.error('Error deleting product:', error));
  };

  // Function to handle clearing the form fields when "Add New Product" button is clicked
  const handleClearForm = () => {
    setEditProduct(null);
    setNewProduct({ name: '', description: '', price: 0, isActive: true });
  };

  // Fetch the list of products when the component mounts
  useEffect(() => {
    fetchProducts();
  }, []);

  return (
    <div>
      <h2>Admin Dashboard</h2>

      {/* Button to add new product */}
      <Button variant="primary" onClick={handleClearForm}>Add New Product</Button>
      <br/>

      {/* Table to display list of products */}
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>Name</th>
            <th>Description</th>
            <th>Price</th>
            <th>Status</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {Array.isArray(products) && products.map((product) => (
            <tr key={product._id}>
              <td>{product.name}</td>
              <td>{product.description}</td>
              <td>${product.price}</td>
              <td>{product.isActive ? 'Active' : 'Inactive'}</td>
              <td>
                <Button variant="primary" onClick={() => handleEditProduct(product)}>
                  Edit
                </Button>{' '}
                <Button
                  variant={product.isActive ? 'danger' : 'success'}
                  onClick={() => handleToggleProduct(product._id, product.isActive)}
                >
                  {product.isActive ? 'Deactivate' : 'Reactivate'}
                </Button>{' '}
                <Button variant="danger" onClick={() => handleDeleteProduct(product._id)}>
                  Delete
                </Button>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>

      {/* Form to add new product or update existing product */}
      <Form onSubmit={handleSubmit}>
        <Form.Group controlId="formName">
          <Form.Label>Product Name</Form.Label>
          <Form.Control
            type="text"
            placeholder="Enter product name"
            value={newProduct.name}
            onChange={(e) => setNewProduct({ ...newProduct, name: e.target.value })}
            required
          />
        </Form.Group>
        <Form.Group controlId="formDescription">
          <Form.Label>Description</Form.Label>
          <Form.Control
            type="text"
            placeholder="Enter product description"
            value={newProduct.description}
            onChange={(e) => setNewProduct({ ...newProduct, description: e.target.value })}
            required
          />
        </Form.Group>
        <Form.Group controlId="formPrice">
          <Form.Label>Price</Form.Label>
          <Form.Control
            type="number"
            placeholder="Enter product price"
            value={newProduct.price}
            onChange={(e) => setNewProduct({ ...newProduct, price: e.target.value })}
            required
          />
        </Form.Group>
        <Form.Group controlId="formStatus">
          <Form.Check
            type="checkbox"
            label="Active"
            checked={newProduct.isActive}
            onChange={(e) => setNewProduct({ ...newProduct, isActive: e.target.checked })}
          />
        </Form.Group>
        <Button variant="primary" type="submit">
          {editProduct ? 'Update Product' : 'Add Product'}
        </Button>
      </Form>
    </div>
  );
};

export default AdminDashboard;
