import Banner from '../components/Banner'

export default function Home() {


const data = {
    title: "Gear 5th: Awakened Gadgets",
    content: "Because pushing buttons is our cardio!",
    destination: "/login",
    label: "Shop now!"
}

	return (
		<>
			<Banner data={data} />

		</>
		
	)
}
