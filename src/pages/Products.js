import ProductCard from '../components/ProductCard';
import { useState, useEffect } from 'react';

export default function Product() {
  const [product, setProduct] = useState([]);

  useEffect(() => {
    fetch("https://cpstn2-ecommerceapi-berol.onrender.com/product/active")
      .then(res => res.json())
      .then(data => {
        setProduct(data);
      });
  }, []);

  return (
    <div>
      <h2>Product</h2>
      <div className="product-list">
        {product.map(product => (
          <ProductCard key={product.id} product={product} />
        ))}
      </div>
    </div>
  );
}
